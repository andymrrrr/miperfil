USE [master]
GO
/****** Object:  Database [Servicio]    Script Date: 13/02/2022 11:01:19 ******/
CREATE DATABASE [Servicio]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Servicio', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Servicio.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Servicio_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Servicio_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Servicio] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Servicio].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Servicio] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Servicio] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Servicio] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Servicio] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Servicio] SET ARITHABORT OFF 
GO
ALTER DATABASE [Servicio] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Servicio] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Servicio] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Servicio] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Servicio] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Servicio] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Servicio] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Servicio] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Servicio] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Servicio] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Servicio] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Servicio] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Servicio] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Servicio] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Servicio] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Servicio] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Servicio] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Servicio] SET RECOVERY FULL 
GO
ALTER DATABASE [Servicio] SET  MULTI_USER 
GO
ALTER DATABASE [Servicio] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Servicio] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Servicio] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Servicio] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Servicio] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Servicio] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Servicio', N'ON'
GO
ALTER DATABASE [Servicio] SET QUERY_STORE = OFF
GO
USE [Servicio]
GO
/****** Object:  Table [dbo].[Caracteristica]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caracteristica](
	[idCaracteristica] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nchar](10) NOT NULL,
 CONSTRAINT [PK_Caracteristica] PRIMARY KEY CLUSTERED 
(
	[idCaracteristica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Curso]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Curso](
	[idCurso] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](80) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Curso] PRIMARY KEY CLUSTERED 
(
	[idCurso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pais]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pais](
	[idPais] [char](10) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Pais] PRIMARY KEY CLUSTERED 
(
	[idPais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prestaciones]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prestaciones](
	[idServicio] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[nombre] [varchar](70) NOT NULL,
	[descripcion] [varchar](500) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Servicio] PRIMARY KEY CLUSTERED 
(
	[idServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proyecto]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proyecto](
	[idProyecto] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[descripcion] [varchar](max) NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Proyecto] PRIMARY KEY CLUSTERED 
(
	[idProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProyectoCaracteristica]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProyectoCaracteristica](
	[idProyectoCaracteristica] [int] IDENTITY(1,1) NOT NULL,
	[idProyecto] [int] NOT NULL,
	[idCaracteristica] [int] NOT NULL,
 CONSTRAINT [PK_ProyectoCaracteristica] PRIMARY KEY CLUSTERED 
(
	[idProyectoCaracteristica] ASC,
	[idProyecto] ASC,
	[idCaracteristica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoUsuario]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoUsuario](
	[idTipoUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_TipoUsuario] PRIMARY KEY CLUSTERED 
(
	[idTipoUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[idRol] [int] NOT NULL,
	[idPais] [char](10) NOT NULL,
	[idTipoUsuario] [int] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[apellido] [varchar](100) NOT NULL,
	[password] [varchar](max) NULL,
	[salt] [varchar](max) NULL,
	[email] [varchar](100) NOT NULL,
	[activo] [bit] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioCurso]    Script Date: 13/02/2022 11:01:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioCurso](
	[idUsuarioCurso] [int] IDENTITY(1,1) NOT NULL,
	[idCurso] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[conocimiento] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_UsuarioCurso] PRIMARY KEY CLUSTERED 
(
	[idUsuarioCurso] ASC,
	[idCurso] ASC,
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Prestaciones]  WITH CHECK ADD  CONSTRAINT [FK_Prestaciones_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[Prestaciones] CHECK CONSTRAINT [FK_Prestaciones_Usuario]
GO
ALTER TABLE [dbo].[Proyecto]  WITH CHECK ADD  CONSTRAINT [FK_Proyecto_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[Proyecto] CHECK CONSTRAINT [FK_Proyecto_Usuario]
GO
ALTER TABLE [dbo].[ProyectoCaracteristica]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoCaracteristica_Caracteristica] FOREIGN KEY([idCaracteristica])
REFERENCES [dbo].[Caracteristica] ([idCaracteristica])
GO
ALTER TABLE [dbo].[ProyectoCaracteristica] CHECK CONSTRAINT [FK_ProyectoCaracteristica_Caracteristica]
GO
ALTER TABLE [dbo].[ProyectoCaracteristica]  WITH CHECK ADD  CONSTRAINT [FK_ProyectoCaracteristica_Proyecto] FOREIGN KEY([idProyecto])
REFERENCES [dbo].[Proyecto] ([idProyecto])
GO
ALTER TABLE [dbo].[ProyectoCaracteristica] CHECK CONSTRAINT [FK_ProyectoCaracteristica_Proyecto]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Pais] FOREIGN KEY([idPais])
REFERENCES [dbo].[Pais] ([idPais])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Pais]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol] FOREIGN KEY([idRol])
REFERENCES [dbo].[Rol] ([idRol])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Rol]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_TipoUsuario] FOREIGN KEY([idTipoUsuario])
REFERENCES [dbo].[TipoUsuario] ([idTipoUsuario])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_TipoUsuario]
GO
ALTER TABLE [dbo].[UsuarioCurso]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioCurso_Curso] FOREIGN KEY([idCurso])
REFERENCES [dbo].[Curso] ([idCurso])
GO
ALTER TABLE [dbo].[UsuarioCurso] CHECK CONSTRAINT [FK_UsuarioCurso_Curso]
GO
ALTER TABLE [dbo].[UsuarioCurso]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioCurso_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[UsuarioCurso] CHECK CONSTRAINT [FK_UsuarioCurso_Usuario]
GO
USE [master]
GO
ALTER DATABASE [Servicio] SET  READ_WRITE 
GO
