﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class Pais
    {
        public Pais()
        {
            Usuarios = new HashSet<Usuario>();
        }
        public string idPais { get; set; }
        [Required(ErrorMessage ="Nombre es Requerido")]
        public string nombre { get; set; }
        public bool activo { get; set; }

        public ICollection<Usuario> Usuarios { get; set; }

    }
}
