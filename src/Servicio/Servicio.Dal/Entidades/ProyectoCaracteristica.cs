﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class ProyectoCaracteristica
    {
        public ProyectoCaracteristica()
        {
            Caracteristicas = new HashSet<Caracteristica>();
        }
        public int idProyectoCaracteristica { get; set; }
        public int idProyecto { get; set; }
        public int idCaracteristica { get; set; }

        public Proyecto Proyecto { get; set; }
        public ICollection<Proyecto> Proyectos { get; set; }

        public Caracteristica Caracteristica { get; set; }
        public ICollection<Caracteristica> Caracteristicas { get; set; }
    }
}
