﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class Usuario
    {
        public Usuario()
        {
            TipoUsuarios = new HashSet<TipoUsuario>();
            Rols = new HashSet<Rol>();
            Paises = new HashSet<Pais>();
            UsuarioCursos = new HashSet<UsuarioCurso>();
            Proyectos = new HashSet<Proyecto>();
        }
        public int idUsuario { get; set; }
        [Required(ErrorMessage ="Seleciona un Rol")]
        public int idRol { get; set; }
        [Required(ErrorMessage ="Selecciona un Pais")]
        public int idPais { get; set; }
        [Required(ErrorMessage ="Seleccion el Tipo de Usuario")]
        public int idTipoUsuario { get; set; }
        [Required(ErrorMessage ="Nombre es Requerido")]
        public string nombre { get; set; }
        [Required(ErrorMessage ="Apellido es Requerido")]
        public string apellido { get; set; }
        public string password { get; set; }
        public string salt { get; set; }
        [Required(ErrorMessage ="Email es Requerido")]
        public string email { get; set; }
        public bool activo { get; set; }
        public DateTime fechaCreacion { get; set; }
        public TipoUsuario TipoUsuario { get; set; }
        public ICollection<TipoUsuario> TipoUsuarios { get; set; }
        public Rol Rol { get; set; }
        public ICollection<Rol> Rols { get; set; }
        public Pais Pais { get; set; }
        public ICollection<Pais> Paises { get; set; }
        public ICollection<UsuarioCurso> UsuarioCursos { get; set; }
        public ICollection<Proyecto> Proyectos { get; set; }
        public ICollection<Prestaciones> Prestaciones { get; set; }

    }
}
