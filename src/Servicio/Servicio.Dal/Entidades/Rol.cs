﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class Rol
    {
        public Rol()
        {
            Usuarios = new HashSet<Usuario>();
        }
        public int idRol { get; set; }
        [Required(ErrorMessage ="Nombre del Rol es Obligatorio")]
        public string nombre { get; set; }
        public bool activo { get; set; }

        public ICollection<Usuario> Usuarios { get; set; }
    }
}
