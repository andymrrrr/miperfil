﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class Prestaciones
    {
        public Prestaciones()
        {
            Usuarios = new HashSet<Usuario>();
        }
        public int idServicio { get; set; }
        public int idUsuario { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public bool activo { get; set; }
        public Usuario Usuario { get; set; }
        public ICollection<Usuario> Usuarios { get; set; }
    }
}
