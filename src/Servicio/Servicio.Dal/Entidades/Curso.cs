﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class Curso
    {
        public Curso()
        {
            UsuarioCursos = new HashSet<UsuarioCurso>();
        }
        public int idCurso { get; set; }
        [Required(ErrorMessage ="Nombre es Requerido")]
        public string nombre { get; set; }
        public bool activo { get; set; }

        public ICollection<UsuarioCurso> UsuarioCursos { get; set; }
    }
}
