﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class Proyecto
    {
        public Proyecto()
        {
            Usuarios = new HashSet<Usuario>();
        }
        public int idProyecto { get; set; }
        public int idUsuario { get; set; }
        [Required(ErrorMessage ="Nombre es Requerido")]
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public DateTime fechaCreacion { get; set; }
        public bool activo { get; set; }

        public Usuario Usuario { get; set; }
        public ICollection<Usuario> Usuarios { get; set; }

        public ICollection<ProyectoCaracteristica> ProyectoCaracteristicas { get; set; }

    }
}
