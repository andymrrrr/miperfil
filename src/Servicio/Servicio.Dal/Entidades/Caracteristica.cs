﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class Caracteristica
    {
        public Caracteristica()
        {
            ProyectoCaracteristicas = new HashSet<ProyectoCaracteristica>();
        }
        public int idCaracteristica { get; set; }
        [Required(ErrorMessage ="Nombre es Requerido")]
        public string nombre { get; set; }

        public ICollection<ProyectoCaracteristica>  ProyectoCaracteristicas { get; set; }
    }
}
