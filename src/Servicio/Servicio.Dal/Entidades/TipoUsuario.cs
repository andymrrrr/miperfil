﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Servicio.Dal.Entidades
{
    public class TipoUsuario
    {
        public TipoUsuario()
        {
            Usuarios = new HashSet<Usuario>();
        }
        public int idTipoUsuario { get; set; }
        [Required(ErrorMessage ="Nombre es Requrido")]
        public string nombre { get; set; }
        public bool activo { get; set; }
        public ICollection<Usuario> Usuarios { get; set; }

    }
}
