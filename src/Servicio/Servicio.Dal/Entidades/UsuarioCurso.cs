﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Servicio.Dal.Entidades
{
   public class UsuarioCurso
    {
        public UsuarioCurso()
        {
            Cursos = new HashSet<Curso>();
            Usuarios = new HashSet<Usuario>();

        }
        public int idUsuarioCurso { get; set; }
        public int idCurso { get; set; }
        public int idUsuario { get; set; }
        public decimal conocimiento { get; set; }
        public Curso Curso { get; set; }
        public ICollection<Curso> Cursos { get; set; }
        public Usuario Usuario { get; set; }
        public ICollection<Usuario> Usuarios { get; set; }
    }
}
