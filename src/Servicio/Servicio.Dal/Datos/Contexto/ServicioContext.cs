﻿using Microsoft.EntityFrameworkCore;
using Servicio.Dal.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Servicio.Dal.Datos.Contexto
{
    public class ServicioContext : DbContext
    {
        public ServicioContext(DbContextOptions<ServicioContext> options) : base(options)
        { }

        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Rol> Rol { get; set; }
        public DbSet<TipoUsuario> TipoUsuario { get; set; }
       // public DbSet<Prestaciones> Prestaciones { get; set; }
        public DbSet<Curso> Curso { get; set; }
        public DbSet<UsuarioCurso> UsuarioCurso { get; set; }
        public DbSet<Proyecto> Proyecto { get; set; }
        public DbSet<ProyectoCaracteristica> ProyectoCaracteristica { get; set; }
        protected override void OnModelCreating(ModelBuilder model)
        {
            model.Entity<Usuario>(entidad =>
            {
                entidad.HasOne(R => R.Rol)
                .WithMany(U => U.Usuarios)
                .HasForeignKey(F => F.idRol)
                .OnDelete(DeleteBehavior.ClientSetNull);

                entidad.HasOne(T => T.TipoUsuario)
                .WithMany(U => U.Usuarios)
                .HasForeignKey(F => F.idTipoUsuario)
                .OnDelete(DeleteBehavior.ClientSetNull);

                entidad.HasOne(P => P.Pais)
                .WithMany(U => U.Usuarios)
                .HasForeignKey(F => F.idPais)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });

            model.Entity<UsuarioCurso>(entidad =>
            {
                entidad.HasOne(C => C.Curso)
                .WithMany(UC => UC.UsuarioCursos)
                .OnDelete(DeleteBehavior.ClientSetNull);

                entidad.HasOne(U => U.Usuario)
                .WithMany(UC => UC.UsuarioCursos)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });

            model.Entity<Proyecto>(entidad =>
            {
                entidad.HasOne(P => P.Usuario)
                .WithMany(U => U.Proyectos)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });

            model.Entity<ProyectoCaracteristica>(entidad =>
            {
                entidad.HasOne(PC => PC.Caracteristica)
                .WithMany(C => C.ProyectoCaracteristicas)
                .OnDelete(DeleteBehavior.ClientSetNull);

                entidad.HasOne(PC => PC.Proyecto)
                .WithMany(P => P.ProyectoCaracteristicas)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });
           
        }
        public override int SaveChanges()
        {
            var entities = from e in ChangeTracker.Entries()
                           where e.State == EntityState.Added
                               || e.State == EntityState.Modified
                           select e.Entity;
            foreach (var entity in entities)
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(
                    entity,
                    validationContext,
                    validateAllProperties: true);
            }

            return base.SaveChanges();
        }
    }
}
