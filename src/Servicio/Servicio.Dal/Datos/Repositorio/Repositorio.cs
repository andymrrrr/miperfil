﻿using Microsoft.EntityFrameworkCore;
using Servicio.Dal.Datos.Contexto;
using Servicio.Dal.Datos.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Servicio.Dal.Datos.Repositorio
{
    public class Repositorio <T> : IRepositorio<T> where T : class
    {
        protected readonly ServicioContext _context;

       
        public Repositorio(ServicioContext context)
        {
            _context = context;
        }

        public void Agregar(T entidad)
        {
            _context.Set<T>().Add(entidad);
        }

        public void AgregarColeccion(T entidad)
        {
            _context.Set<T>().AddRange(entidad);
        }
        public void Eliminar(T entidad)
        {
            T existe = _context.Set<T>().Find(entidad);
            if (existe != null) _context.Set<T>().Remove(existe);
        }
        #region Metodos Asincrono
        public async Task<List<T>> BuscarAsinc(Expression<Func<T, bool>> accion)
        {
            return await _context.Set<T>().Where(accion).ToListAsync();
        }
        public async Task<List<T>> BuscarTodoAsinc()
        {
            return await _context.Set<T>().ToListAsync();
        }


        public virtual async Task<T> BuscarPorIdAsinc(int Id)
        {
            return await _context.Set<T>().FindAsync(Id);
        }
        #endregion
        public IEnumerable<T> BuscarTodo()
        {
            return _context.Set<T>().AsEnumerable();
        }
        public IEnumerable<T> Buscar()
        {
            return _context.Set<T>().AsNoTracking().AsEnumerable<T>();
        }

        public IEnumerable<T> Buscar(Expression<Func<T, bool>> accion)
        {
            return _context.Set<T>().Where(accion).AsNoTracking().AsEnumerable<T>();
        }
        public T BuscarPorId(int Id)
        {
            return _context.Set<T>().Find(Id);
        }
        public IQueryable<T> Consultar()
        {
            var consulta = _context.Set<T>().AsEnumerable<T>();
            return consulta.AsQueryable();
        }
        public IQueryable<T> Consultar(Expression<Func<T, bool>> accion)
        {
            var consulta = _context.Set<T>().Where(accion).AsEnumerable<T>();
            return consulta.AsQueryable();
        }
        public void Actualizar(T entidad)
        {
            _context.Set<T>().Update(entidad);
        }


    }
}
