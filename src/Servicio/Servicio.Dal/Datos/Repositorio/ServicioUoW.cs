﻿using Servicio.Dal.Datos.Contexto;
using Servicio.Dal.Datos.Interfaz;
using Servicio.Dal.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Servicio.Dal.Datos.Repositorio
{
    public class ServicioUoW : IServicioUoW
    {
        private readonly ServicioContext _context;

        public IRepositorio<Caracteristica> Caracteristica { get; set; }
        public IRepositorio<Curso> Curso { get; set; }
        public IRepositorio<Pais> Pais { get; set; }
        public IRepositorio<Proyecto> Proyecto { get; set; }
        public IRepositorio<ProyectoCaracteristica> ProyectoCaracteristica { get; set; }
        public IRepositorio<Rol> Rol { get; set; }
        public IRepositorio<TipoUsuario> TipoUsuario { get; set; }
        public IRepositorio<Usuario> Usuario { get; set; }
        public IRepositorio<UsuarioCurso> UsuarioCurso { get; set; }

        public ServicioUoW(ServicioContext context)
        {
            _context = context;
            Caracteristica = new Repositorio<Caracteristica>(context);
            Curso = new Repositorio<Curso>(context);
            Pais = new Repositorio<Pais>(context);
            Proyecto = new Repositorio<Proyecto>(context);
            ProyectoCaracteristica = new Repositorio<ProyectoCaracteristica>(context);
            Rol = new Repositorio<Rol>(context);
            TipoUsuario = new Repositorio<TipoUsuario>(context);
            Usuario = new Repositorio<Usuario>(context);
            UsuarioCurso = new Repositorio<UsuarioCurso>(context);

        }
        public void GuardarCambios()
        {
            _context.SaveChanges();
        }
        public async Task GuardarCambiosAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();

        }
    }
}
