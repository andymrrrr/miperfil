﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Servicio.Dal.Datos.Interfaz
{
    public interface IRepositorio<T> where T : class
    {
        Task<T> BuscarPorIdAsinc(int Id);
        Task<List<T>> BuscarTodoAsinc();
        Task<List<T>> BuscarAsinc(Expression<Func<T, bool>> predicate);
        IEnumerable<T> BuscarTodo();
        IEnumerable<T> Buscar();
        IEnumerable<T> Buscar(Expression<Func<T, bool>> predicate);
        T BuscarPorId(int Id);
        IQueryable<T> Consultar();
        IQueryable<T> Consultar(Expression<Func<T, bool>> predicate);
        void Agregar(T entity);
        void Eliminar(T entity);
        void Actualizar(T entity);
    }
}
