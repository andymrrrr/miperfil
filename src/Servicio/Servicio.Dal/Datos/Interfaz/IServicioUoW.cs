﻿using Servicio.Dal.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Servicio.Dal.Datos.Interfaz
{
    public interface IServicioUoW : IDisposable
    {
        IRepositorio<Usuario> Usuario { get; set; }
        IRepositorio<Rol> Rol { get; set; }
        IRepositorio<Pais> Pais { get; set; }
        IRepositorio<TipoUsuario> TipoUsuario { get; set; }
      //  IRepositorio<Servicio.Dal.Entidades.Prestaciones> Servicio { get; set; }
        IRepositorio<Curso> Curso { get; set; }
        IRepositorio<UsuarioCurso> UsuarioCurso { get; set; }
        IRepositorio<Caracteristica> Caracteristica { get; set; }
        IRepositorio<Proyecto> Proyecto { get; set; }
        IRepositorio<ProyectoCaracteristica> ProyectoCaracteristica { get; set; }
        void GuardarCambios();
        Task GuardarCambiosAsync();
    }
}
